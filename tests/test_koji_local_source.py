# -*- coding: utf-8 -*-
import os
import ddt
import unittest

from pyfakefs.fake_filesystem_unittest import TestCase

from pungi.wrappers.kojimock import KojiMock, RELEASE_BUILD_ID

PATH_TO_REPOS = '/path/to/repos'
MODULES_YAML_GZ = 'modules.yaml.gz'


@ddt.ddt
class TestLocalKojiMock(TestCase):
    maxDiff = None

    FILES_TO_CREATE = [
        # modular package that should be excluded from global list
        'powertools/Packages/ant-1.10.5-1.module_el8.0.0+30+832da3a1.noarch.rpm',
        # packages that should be gathered
        'powertools/Packages/libgit2-devel-0.26.8-2.el8.x86_64.rpm',
        'appstream/Packages/OpenEXR-devel-2.2.0-11.el8.i686.rpm',
        'appstream/Packages/mingw-binutils-generic-2.30-1.el8.x86_64.rpm',
        # non-rpm
        'appstream/Packages/somenonrpm',
    ]

    MARIADB_MODULE = """
---
document: modulemd
version: 2
data:
  name: mariadb-devel
  stream: 10.3
  version: 8010020200108182321
  context: cdc1202b
  arch: x86_64
  summary: MariaDB Module
  license:
    content:
      - (CDDL or GPLv2 with exceptions) and ASL 2.0
    module:
      - MIT
  description: >-
    MariaDB is a community developed branch of MySQL. 
  components:
    rpms:
      Judy:
        rationale: MariaDB dependency for OQgraph computation engine
        ref: a3583b33f939e74a530f2a1dff0552dff2c8ea73
        buildorder: 4
        arches: [aarch64, i686, ppc64le, x86_64]
  artifacts:
    rpms:
    - Judy-0:1.0.5-18.module_el8.1.0+217+4d875839.i686
    - Judy-debuginfo-0:1.0.5-18.module_el8.1.0+217+4d875839.i686
    """

    JAVAPACKAGES_TOOLS_MODULE = """
---
document: modulemd
version: 2
data:
  name: javapackages-tools
  stream: 201801
  version: 8000020190628172923
  context: b07bea58
  arch: x86_64
  summary: Tools and macros for Java packaging support
  license:
    content:
      - (CDDL or GPLv2 with exceptions) and ASL 2.0
    module:
      - MIT
  description: >-
    Java Packages Tools is a collection of tools that make it easier to build RPM
    packages containing software running on Java platform.
  components:
    rpms:
      ant:
        rationale: "Runtime dependency of ant-contrib"
        ref: 2eaf095676540e2805ee7e8c7f6f78285c428fdc
        arches: [aarch64, i686, ppc64le, x86_64]
  artifacts:
    rpms:
    - ant-0:1.10.5-1.module_el8.0.0+30+832da3a1.noarch
    - ant-0:1.10.5-1.module_el8.0.0+30+832da3a1.src
    """

    ANT_DEFAULTS = """
data:
  module: ant
  profiles:
    '1.10':
    - common
  stream: '1.10'
document: modulemd-defaults
version: '1'
    """

    def setUp(self):
        self.setUpPyfakefs()

        os.makedirs(PATH_TO_REPOS)
        os.makedirs(os.path.join(PATH_TO_REPOS, 'modules/x86_64'))

        with open(os.path.join(PATH_TO_REPOS, 'modules/x86_64',
                               'javapackages-tools-201801-8000020190628172923.b07bea58'), 'w') as f:
            f.write(self.JAVAPACKAGES_TOOLS_MODULE)

        with open(os.path.join(PATH_TO_REPOS, 'modules/x86_64',
                               'mariadb-devel-10.3-8010020200108182321.cdc1202b'), 'w') as f:
            f.write(self.MARIADB_MODULE)

        for filepath in self.FILES_TO_CREATE:
            os.makedirs(os.path.join(PATH_TO_REPOS, os.path.dirname(filepath)), exist_ok=True)
            open(os.path.join(PATH_TO_REPOS, filepath), 'w').close()

        self._koji = KojiMock(
            PATH_TO_REPOS,
            os.path.join(PATH_TO_REPOS, 'modules'),
            ['x86_64', 'noarch', 'i686'],
        )

    @ddt.data(
        [0, {
            'completion_ts': 0,
            'arch': 'x86_64',
            'extra': {
                'typeinfo': {
                    'module': {
                        'content_koji_tag': 'javapackages-tools-201801-8000020190628172923.b07bea58',
                        'context': 'b07bea58',
                        'name': 'javapackages-tools',
                        'stream': '201801',
                        'version': '8000020190628172923'
                    }
                }
            },
            'id': 0,
            'name': 'javapackages-tools',
            'release': '8000020190628172923.b07bea58',
            'state': 'COMPLETE',
            'version': '201801'
        }],
        [1, {
            'completion_ts': 0,
            'arch': 'x86_64',
            'extra': {
                'typeinfo': {
                    'module': {
                        'content_koji_tag': 'mariadb-devel-10.3-8010020200108182321.cdc1202b',
                        'context': 'cdc1202b',
                        'name': 'mariadb-devel',
                        'stream': '10.3',
                        'version': '8010020200108182321'
                    }
                }
            },
            'id': 1,
            'name': 'mariadb-devel',
            'release': '8010020200108182321.cdc1202b',
            'state': 'COMPLETE',
            'version': '10.3'
        }]
    )
    @ddt.unpack
    def test_get_build_info(self, build_id, result):
        """
        Check that we are able to get build information from getBuild method
        """
        build_info = self._koji.getBuild(build_id)

        self.assertEqual(result, build_info)

    @ddt.data(
        [0, [{'btype': 'module', 'build_id': 0, 'filename': 'modulemd.x86_64.txt'},
             {'btype': 'module', 'build_id': 0, 'filename': 'modulemd.txt'}]],
        [1, [{'btype': 'module', 'build_id': 1, 'filename': 'modulemd.x86_64.txt'},
             {'btype': 'module', 'build_id': 1, 'filename': 'modulemd.txt'}]]
    )
    @ddt.unpack
    def test_list_archives(self, build_id, result):
        """
        Provides list of archives of module descriptions.
        Always should contain at least two files, so
        I did a little hack and added modulemd.txt (it is on real koji)
        but it is not used later by pungi
        """
        build_info = self._koji.listArchives(build_id)

        self.assertEqual(result, build_info)

    @ddt.data(
        [
            'javapackages-tools-201801-8000020190628172923.b07bea58',
            [
                [
                    {
                        'arch': 'noarch',
                        'build_id': 0,
                        'epoch': '0',
                        'extra': None,
                        'id': 262555,
                        'metadata_only': False,
                        'name': 'ant',
                        'release': '1.module_el8.0.0+30+832da3a1',
                        'size': 0,
                        'version': '1.10.5'
                    },
                    {
                        'arch': 'src',
                        'build_id': 0,
                        'epoch': '0',
                        'extra': None,
                        'id': 262555,
                        'metadata_only': False,
                        'name': 'ant',
                        'release': '1.module_el8.0.0+30+832da3a1',
                        'size': 0,
                        'version': '1.10.5'
                    }
                ],
                [
                    {
                        'build_id': 0,
                        'id': 0,
                        'name': 'javapackages-tools',
                        'nvr': 'javapackages-tools-201801-8000020190628172923.b07bea58',
                        'package_name': 'javapackages-tools',
                        'release': '8000020190628172923',
                        'tag_name': 'javapackages-tools-201801-8000020190628172923.b07bea58',
                        'version': '201801',
                        'volume_name': 'DEFAULT'
                    }
                ]
            ]
        ],
        [
            'mariadb-devel-10.3-8010020200108182321.cdc1202b',
            [
                [
                    {
                        'arch': 'i686',
                        'build_id': 1,
                        'epoch': '0',
                        'extra': None,
                        'id': 262555,
                        'metadata_only': False,
                        'name': 'Judy',
                        'release': '18.module_el8.1.0+217+4d875839',
                        'size': 0,
                        'version': '1.0.5'
                    },
                    {
                        'arch': 'i686',
                        'build_id': 1,
                        'epoch': '0',
                        'extra': None,
                        'id': 262555,
                        'metadata_only': False,
                        'name': 'Judy-debuginfo',
                        'release': '18.module_el8.1.0+217+4d875839',
                        'size': 0,
                        'version': '1.0.5'
                    }
                ],
                [
                    {'build_id': 1,
                     'id': 1,
                     'name': 'mariadb-devel',
                     'nvr': 'mariadb-devel-10.3-8010020200108182321.cdc1202b',
                     'package_name': 'mariadb-devel',
                     'release': '8010020200108182321',
                     'tag_name': 'mariadb-devel-10.3-8010020200108182321.cdc1202b',
                     'version': '10.3',
                     'volume_name': 'DEFAULT'
                     }
                ]
            ]
        ],
        [
            'dist-c8-compose',
            [
                [
                    {
                        'arch': 'x86_64',
                        'build_id': RELEASE_BUILD_ID,
                        'epoch': None,
                        'extra': None,
                        'metadata_only': False,
                        'name': 'libgit2-devel',
                        'release': '2.el8',
                        'version': '0.26.8'
                    },
                    {
                        'arch': 'i686',
                        'build_id': RELEASE_BUILD_ID,
                        'epoch': None,
                        'extra': None,
                        'metadata_only': False,
                        'name': 'OpenEXR-devel',
                        'release': '11.el8',
                        'version': '2.2.0'
                    },
                    {
                        'arch': 'x86_64',
                        'build_id': RELEASE_BUILD_ID,
                        'epoch': None,
                        'extra': None,
                        'metadata_only': False,
                        'name': 'mingw-binutils-generic',
                        'release': '1.el8',
                        'version': '2.30'
                    }
                ],
                # no build needed in this case because pungi does not use them
                []
            ]
        ],
    )
    @ddt.unpack
    def test_list_tagged_rpms(self, tag, result):
        """
        This method is used by pungi to get list of rpms:
        either modular or just prepared for release
        """
        self.assertEqual(result, self._koji.listTaggedRPMS(tag))

    def test_list_tagged(self):
        """
        Used only to get list of modules for some release.
        """
        result = self._koji.listTagged('dist-c8-module-compose')
        self.assertEqual([
            {
                'arch': 'x86_64',
                'build_id': 0,
                'id': 0,
                'name': 'javapackages-tools',
                'nvr': 'javapackages-tools-201801-8000020190628172923.b07bea58',
                'owner_name': 'centos',
                'package_name': 'javapackages-tools',
                'release': '8000020190628172923.b07bea58',
                'tag_name': 'dist-c8-module-compose',
                'version': '201801'
            },
            {
                'arch': 'x86_64',
                'build_id': 1,
                'id': 1,
                'name': 'mariadb-devel',
                'nvr': 'mariadb-devel-10.3-8010020200108182321.cdc1202b',
                'owner_name': 'centos',
                'package_name': 'mariadb-devel',
                'release': '8010020200108182321.cdc1202b',
                'tag_name': 'dist-c8-module-compose',
                'version': '10.3'
            }], result)


if __name__ == '__main__':
    unittest.main()
