# -*- coding: utf-8 -*-
import os
import unittest
from pathlib import Path

from pyfakefs.fake_filesystem_unittest import TestCase

from pungi.scripts.gather_rpms import search_rpms, copy_rpms, Package
from productmd.common import parse_nvra

PATH_TO_REPOS = '/path/to/repos'
MODULES_YAML_GZ = 'modules.yaml.gz'


class TestGatherRpms(TestCase):
    maxDiff = None

    FILES_TO_CREATE = [
        'powertools/Packages/libvirt-6.0.0-28.module_el'
        '8.3.0+555+a55c8938.i686.rpm',
        'powertools/Packages/libgit2-devel-0.26.8-2.el8.x86_64.rpm',
        'powertools/Packages/xalan-j2-2.7.1-38.module_el'
        '8.0.0+30+832da3a1.noarch.rpm',
        'appstream/Packages/bnd-maven-plugin-3.5.0-4.module_el'
        '8.0.0+30+832da3a1.noarch.rpm',
        'appstream/Packages/OpenEXR-devel-2.2.0-11.el8.i686.rpm',
        'appstream/Packages/mingw-binutils-generic-2.30-1.el8.x86_64.rpm',
        'appstream/Packages/somenonrpm',
    ]

    def setUp(self):
        self.setUpPyfakefs()

        os.makedirs(PATH_TO_REPOS)

        for filepath in self.FILES_TO_CREATE:
            os.makedirs(
                os.path.join(PATH_TO_REPOS, os.path.dirname(filepath)),
                exist_ok=True,
            )
            open(os.path.join(PATH_TO_REPOS, filepath), 'w').close()

    def test_gather_rpms(self):
        self.assertEqual(
            [Package(nvra=parse_nvra('libvirt-6.0.0-28.module_'
                                     'el8.3.0+555+a55c8938.i686'),
                     path=Path(
                         f'{PATH_TO_REPOS}/powertools/Packages/'
                         f'libvirt-6.0.0-28.module_el'
                         f'8.3.0+555+a55c8938.i686.rpm'
                     )),
             Package(nvra=parse_nvra('libgit2-devel-0.26.8-2.el8.x86_64'),
                     path=Path(
                         f'{PATH_TO_REPOS}/powertools/Packages/'
                         f'libgit2-devel-0.26.8-2.el8.x86_64.rpm'
                     )),
             Package(nvra=parse_nvra('xalan-j2-2.7.1-38.module_el'
                          '8.0.0+30+832da3a1.noarch'),
                     path=Path(
                         f'{PATH_TO_REPOS}/powertools/Packages/'
                         f'xalan-j2-2.7.1-38.module_el'
                         f'8.0.0+30+832da3a1.noarch.rpm'
                     )),
             Package(nvra=parse_nvra('bnd-maven-plugin-3.5.0-4.module_el'
                          '8.0.0+30+832da3a1.noarch'),
                     path=Path(
                         '/path/to/repos/appstream/Packages/'
                         'bnd-maven-plugin-3.5.0-4.module_el'
                         '8.0.0+30+832da3a1.noarch.rpm'
                     )),
             Package(nvra=parse_nvra('OpenEXR-devel-2.2.0-11.el8.i686'),
                     path=Path(
                         f'{PATH_TO_REPOS}/appstream/Packages/'
                         f'OpenEXR-devel-2.2.0-11.el8.i686.rpm'
                     )),
             Package(nvra=parse_nvra('mingw-binutils-generic-'
                                     '2.30-1.el8.x86_64'),
                     path=Path(
                         f'{PATH_TO_REPOS}/appstream/Packages/'
                         f'mingw-binutils-generic-2.30-1.el8.x86_64.rpm'
                     ))
             ],
            search_rpms(Path(PATH_TO_REPOS))
        )

    def test_copy_rpms(self):
        target_path = Path('/mnt/koji')
        packages = [

            Package(nvra=parse_nvra('libvirt-6.0.0-28.module_'
                                    'el8.3.0+555+a55c8938.i686'),
                    path=Path(
                        f'{PATH_TO_REPOS}/powertools/Packages/'
                        f'libvirt-6.0.0-28.module_el'
                        f'8.3.0+555+a55c8938.i686.rpm'
                    )),
            Package(nvra=parse_nvra('libgit2-devel-0.26.8-2.el8.x86_64'),
                    path=Path(
                        f'{PATH_TO_REPOS}/powertools/Packages/'
                        f'libgit2-devel-0.26.8-2.el8.x86_64.rpm'
                    )),
            Package(nvra=parse_nvra('xalan-j2-2.7.1-38.module_'
                                    'el8.0.0+30+832da3a1.noarch'),
                    path=Path(
                        f'{PATH_TO_REPOS}/powertools/Packages/'
                        f'xalan-j2-2.7.1-38.module_el'
                        f'8.0.0+30+832da3a1.noarch.rpm'
                    )),
            Package(nvra=parse_nvra('bnd-maven-plugin-3.5.0-4.module_el'
                         '8.0.0+30+832da3a1.noarch'),
                    path=Path(
                        '/path/to/repos/appstream/Packages/'
                         'bnd-maven-plugin-3.5.0-4.module_el'
                        '8.0.0+30+832da3a1.noarch.rpm'
                    )),
            Package(nvra=parse_nvra('OpenEXR-devel-2.2.0-11.el8.i686'),
                    path=Path(
                        f'{PATH_TO_REPOS}/appstream/Packages/'
                        f'OpenEXR-devel-2.2.0-11.el8.i686.rpm'
                    )),
            Package(nvra=parse_nvra('mingw-binutils-generic-'
                                    '2.30-1.el8.x86_64'),
                    path=Path(
                        f'{PATH_TO_REPOS}/appstream/Packages/'
                        f'mingw-binutils-generic-2.30-1.el8.x86_64.rpm'
                    ))
        ]
        copy_rpms(packages, target_path, [])

        self.assertCountEqual([
            'xalan-j2-2.7.1-38.module_el8.0.0+30+832da3a1.noarch.rpm',
            'bnd-maven-plugin-3.5.0-4.module_el8.0.0+30+832da3a1.noarch.rpm'
        ], os.listdir(target_path / 'noarch'))

        self.assertCountEqual([
            'libgit2-devel-0.26.8-2.el8.x86_64.rpm',
            'mingw-binutils-generic-2.30-1.el8.x86_64.rpm'
        ], os.listdir(target_path / 'x86_64'))

        self.assertCountEqual([
            'libvirt-6.0.0-28.module_el8.3.0+555+a55c8938.i686.rpm',
            'OpenEXR-devel-2.2.0-11.el8.i686.rpm'
        ], os.listdir(target_path / 'i686'))

        self.assertCountEqual([
            'i686', 'x86_64', 'noarch'
        ], os.listdir(target_path))


if __name__ == '__main__':
    unittest.main()
