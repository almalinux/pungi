# coding=utf-8

import os
from collections import defaultdict
from unittest import TestCase, mock, main

from pungi.scripts.create_packages_json import (
    PackagesGenerator,
    RepoInfo,
    VariantInfo,
)

FOLDER_WITH_TEST_DATA = os.path.join(
    os.path.dirname(
        os.path.abspath(__file__)
    ),
    'data/test_create_packages_json/',
)

test_repo_info = RepoInfo(
    path=FOLDER_WITH_TEST_DATA,
    folder='test_repo',
    is_remote=False,
    is_reference=True,
)

test_repo_info_2 = RepoInfo(
    path=FOLDER_WITH_TEST_DATA,
    folder='test_repo_2',
    is_remote=False,
    is_reference=True,
)
variant_info_1 = VariantInfo(
    name='TestRepo',
    arch='x86_64',
    repos=[test_repo_info]
)
variant_info_2 = VariantInfo(
    name='TestRepo2',
    arch='x86_64',
    repos=[test_repo_info_2]
)


class TestPackagesJson(TestCase):
    def test_01_get_remote_file_content(self):
        """
        Test the getting of content from a remote file
        """
        request_object = mock.Mock()
        request_object.raise_for_status = lambda: True
        request_object.content = b'TestContent'
        with mock.patch(
            'pungi.scripts.create_packages_json.requests.get',
            return_value=request_object,
        ) as mock_requests_get, mock.patch(
            'pungi.scripts.create_packages_json.tempfile.NamedTemporaryFile',
        ) as mock_tempfile:
            mock_tempfile.return_value.__enter__.return_value.name = 'tmpfile'
            packages_generator = PackagesGenerator(
                variants=[],
                excluded_packages=[],
                included_packages=[],
            )
            file_name = packages_generator.get_remote_file_content(
                file_url='fakeurl')
            mock_requests_get.assert_called_once_with(url='fakeurl')
            mock_tempfile.assert_called_once_with(delete=False)
            mock_tempfile.return_value.__enter__().\
                write.assert_called_once_with(b'TestContent')
            self.assertEqual(
                file_name,
                'tmpfile',
            )

    def test_02_generate_additional_packages(self):
        pg = PackagesGenerator(
            variants=[
                variant_info_1,
                variant_info_2,
            ],
            excluded_packages=['zziplib-utils'],
            included_packages=['vim-file*'],
        )
        test_packages = defaultdict(
            lambda: defaultdict(
                lambda: defaultdict(
                    list,
                )
            )
        )
        test_packages['TestRepo']['x86_64']['zziplib'] = \
            [
                'zziplib.i686',
                'zziplib.x86_64',
            ]
        test_packages['TestRepo2']['x86_64']['vim'] = \
            [
                'vim-X11.i686',
                'vim-common.i686',
                'vim-enhanced.i686',
                'vim-filesystem.noarch',
            ]
        result = pg.generate_packages_json()
        self.assertEqual(
            test_packages,
            result,
        )


if __name__ == '__main__':
    main()
